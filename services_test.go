package envix_test

import (
	"fmt"
	"testing"

	"bitbucket.org/sikharis/envix"
	"github.com/gookit/color"
)

func TestString(t *testing.T) {

	var svc envix.Services

	var tests = []struct {
		in  envix.Service
		out string
	}{
		{envix.UNKNOWN, "Service Unknown"},
		{envix.MSMQ, "Message Queuing"},
		{envix.MSDTC, "Distributed Transaction Coordinator"},
	}

	for _, val := range tests {
		svc.Service = val.in
		res := svc.Service.String()
		if res != val.out {
			t.Errorf("got: \"%s\" want: \"%s\"\n", res, val.out)
		}
	}
}

func TestGet(t *testing.T) {

	target := envix.Target{
		Address:  "magnum",
		User:     "magnum\\Administrator",
		Password: "AdIns2015",
	}

	var tests = []struct {
		in  envix.Services
		out bool
	}{
		{
			envix.Services{
				Service: envix.UNKNOWN,
				Target:  target,
			},
			false,
		},
		{
			envix.Services{
				Service: envix.MSDTC,
				Target:  target,
			},
			true,
		},
		{
			envix.Services{
				Service: envix.MSMQ,
				Target:  target,
			},
			false,
		},
	}

	for _, val := range tests {
		res, err := val.in.Get()

		// fmt.Printf("%v", err)

		// isError(err)
		if res.Active != val.out {
			if err != nil {
				t.Errorf("got: \"%t\" want: \"%t\" error: %s\n", res.Active, val.out, err.Error())
			} else {
				t.Errorf("got: \"%t\" want: \"%t\"\n", res.Active, val.out)
			}
		}
	}
}

func isError(err error, msg ...string) bool {
	if err != nil {
		color.New(color.FgWhite, color.BgRed).Println(fmt.Sprint(err.Error()) + ": " + fmt.Sprintf("%v", err))
	}

	return (err != nil)
}
