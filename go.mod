module bitbucket.org/sikharis/envix

go 1.13

require (
	github.com/gookit/color v1.2.0
	github.com/jinzhu/copier v0.0.0-20190625015134-976e0346caa8 // indirect
)
