package envix

import (
	"errors"
	"fmt"
	"os/exec"
	"strings"
)

//Service is collection of available service
type Service int

const (
	//UNKNOWN Service
	UNKNOWN Service = 0

	//MSMQ Service
	MSMQ Service = 1

	//MSDTC Service
	MSDTC Service = 2
)

//Services is struct for contain service info
type Services struct {
	Service Service
	Active  bool
	Target  Target
	Query   Query
	Debug   bool
}

//Query is struct  for contain command and result of os execute
type Query struct {
	Command string
	Result  []byte
}

//Target is struct to save target information & credential
type Target struct {
	Address  string
	User     string
	Password string
}

// type Endpoints struct {
// 	Protocol string `json:"protocol"`
// 	Path     string `json:"path"`
// }

// type Environment struct {
// 	Name       string     `json:"name"`
// 	Address    string     `json:"address"`
// 	Credential Credential `json:"credential"`
// }

//String is method to get service name in string datatype
func (svc Service) String() string {
	//Arrays of service list
	names := [...]string{
		"Service Unknown",
		"Message Queuing",
		"Distributed Transaction Coordinator",
	}
	// → `svc`: It's one of the
	// values of Service constants.
	// If the constant is MSMQ,
	// then day is 0.
	//
	// prevent panicking in case of
	// `svc` is out of range of Service
	if svc < UNKNOWN || svc > MSDTC {
		return "Service Unknown"
	}
	// return the name of a Service
	// constant from the names array
	// above.
	return names[svc]
}

//ExecutableName is method to get executable name in string datatype
func (svc Service) ExecutableName() string {
	//Arrays of service list
	names := [...]string{
		"Service Unknown",
		"mqsvc.exe",
		"msdtc.exe",
	}
	// → `svc`: It's one of the
	// values of Service constants.
	// If the constant is MSMQ,
	// then day is 0.
	//
	// prevent panicking in case of
	// `svc` is out of range of Service
	if svc < UNKNOWN || svc > MSDTC {
		return "Service Unknown"
	}
	// return the name of a Service
	// constant from the names array
	// above.
	return names[svc]
}

//Get is method to get service information
func (svc Services) Get() (Services, error) {
	switch svc.Service {
	case MSMQ, MSDTC: //Get service info using WMI
		var err error

		//Command string
		command := fmt.Sprintf("wmic /user:'%s' /password:'%s' /node:'%s' process where name='%s' get ProcessId",
			svc.Target.User, svc.Target.Password, svc.Target.Address, svc.Service.ExecutableName())

		//Execute command
		result, err := execCMD(command)

		svc.Query.Command = command
		svc.Query.Result = result

		//Debug
		// fmt.Printf("%s\n %s", command, string(result))

		//Check status
		if strings.Contains(string(result), "ProcessId") {
			svc.Active = true
		} else {
			svc.Active = false
		}

		return svc, err
	case UNKNOWN: //Get service info using WMI
		return svc, errors.New("Service unknown")
	default: // If svc is not a service
		var info Services
		var err error
		return info, err
	}
}

func execCMD(command string) ([]byte, error) {
	args := strings.Fields(command)
	cmd := exec.Command(args[0], args[1:len(args)]...)
	return cmd.CombinedOutput()
}

// //New is method to create instance
// func New() (Services, error) {
// 	command := "wmic"
// 	args := strings.Fields(command)
// 	cmd := exec.Command(args[0], args[1:len(args)]...)
// 	return cmd.CombinedOutput()
// }
